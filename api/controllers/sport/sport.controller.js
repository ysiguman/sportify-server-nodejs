'use strict';
const SportModel = require('../../models/sport.model');

const Sport = {
    getAll: async function (req, res) {
        const data = await SportModel.getAll();

        if (data.error)
            console.log(data.error);

        return res.json(data);
    },

    get: async (req, res) => {
        const data = await SportModel.get(req.params.id);

        if (data.error)
            console.log(data.error);

        return res.json(data);
    }
};

module.exports = Sport;