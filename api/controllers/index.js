'use strict';
const { Router } = require('express');
const router = Router();


var users = require('./user/user.routing');
var sports = require('./sport/sport.routing');
var events = require('./event/event.routing');

router.use('/users', users);
router.use('/sports', sports);
router.use('/events', events);

module.exports = router;