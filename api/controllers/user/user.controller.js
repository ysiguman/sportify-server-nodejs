'use strict';
const UserModel = require('../../models/user.model');

const User = {
    getAll: async (req, res) => {
        const data = await UserModel.getAll();

        if (data.error)
            console.log(data.error);

        return res.json(data);
    },

    get: async (req, res) => {
        const data = await UserModel.get(req.params.id);

        if (data.error)
            console.log(data.error);

        return res.json(data);
    },

    getSports: async (req, res) => {
        const data = await UserModel.getSportsByUser(req.params.id);

        if (data.error)
            console.log(data.error);

        return res.json(data);
    },

    create: async (req, res) => {
        const result = await UserModel.create(req.body);

        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        res.sendStatus(200).send(req.body.userId);
    },

    addSport: async (req, res) => {
        const result = await UserModel.addSport(req.params.id, req.body);

        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        res.sendStatus(200);
    },

    connect: async (req, res) => {
        const connection = await UserModel.connect(req.body.mail, req.body.password);

        if (connection.error || connection.length !== 1) {
            return res.sendStatus(500);
        }
        return res.status(200).json(connection[0]);
    },

    getEvents: async (req, res) => {
        const connection = await UserModel.getEvents(req.params.id);

        if (connection.error) {
            console.log(connection.error);
            return res.sendStatus(500);
        }
        return res.status(200).json(connection);
    },
    getEventsForUser: async (req, res) => {
        const connection = await UserModel.getEventsForUser(req.params.id);

        if (connection.error) {
            console.log(connection.error);
            return res.sendStatus(500);
        }
        return res.status(200).json(connection);
    }
};

module.exports=User;