'use strict';
const { Router } = require('express');

var user = require('./user.controller');


const router = Router();


// user Routes
router.get('/', user.getAll);
router.get('/:id', user.get);
router.get('/:id/sports', user.getSports);
router.get('/:id/events', user.getEvents);
router.get('/:id/allEvents', user.getEventsForUser);
router.post('/:id/sports', user.addSport);
router.post('/', user.create);
router.post('/connect', user.connect);

module.exports = router;