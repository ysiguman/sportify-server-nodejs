'use strict';
const EventModel = require('../../models/event.model');

const Event = {
    getAll: async (req, res) => {
        const data = await EventModel.getAll();

        if (data.error)
            console.log(data.error);

        return res.json(data);
    },

    get: async (req, res) => {
        const data = await EventModel.get(req.params.id);

        if (data.error)
            console.log(data.error);

        return res.json(data);
    },

    create: async (req, res) => {
        const result = await EventModel.create(JSON.parse(req.body.event), JSON.parse(req.body.userId));

        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        await EventModel.addPrincipal(result, {'user_id': JSON.parse(req.body.userId)})
        res.sendStatus(200);
    },

    getUsers: async (req, res) => {
        const data = await EventModel.getUserOfEvent(req.params.id);

        if (data.error)
            console.log(data.error);

        return res.json(data);
    },

    addUser: async (req, res) => {
        const result = await EventModel.addUser(req.params.id, req.body);
        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        res.sendStatus(200);
    },

    removeUser: async (req, res) => {
        const result = await EventModel.removeUser(req.params.id, req.body);
        console.log(req.body);
        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        res.sendStatus(200);
    },

    acceptUser:  async (req, res) => {
        const result = await EventModel.acceptUser(req.params.id, req.body);

        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        console.log(result);

        res.sendStatus(200);
    },
    revocUser: async (req, res) => {
        const result = await EventModel.revocUser(req.params.id, req.body);

        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        res.sendStatus(200);
    },
    delete: async (req, res) => {
        const result = await EventModel.delete(req.params.id);

        if (result.error) {
            console.log(result.error);
            res.sendStatus(500);
        }

        res.sendStatus(200);
    },
};

module.exports=Event;