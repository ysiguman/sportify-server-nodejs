'use strict';
const { Router } = require('express');

var event = require('./event.controller');

var multer  = require('multer')
var upload = multer()

const router = Router();


// user Routes
router.get('/', event.getAll);
router.get('/:id', event.get);
router.delete('/:id', event.delete);
router.post('/', upload.array(), event.create);
router.get('/:id/users', event.getUsers);
router.post('/:id/add-user', upload.single(), event.addUser);
router.post('/:id/remove-user', upload.single(), event.removeUser);
router.post('/:id/accept-user', upload.single(), event.acceptUser);
router.post('/:id/revoc-user', upload.single(), event.revocUser);

module.exports = router;