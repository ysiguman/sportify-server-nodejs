'use strict';

const 	express = require('express'),
		bodyParser = require('body-parser');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var controllers = require('./controllers/index');


app.use('/api/', controllers);


app.listen(PORT, HOST);
console.log(`Running on => http://${HOST}:${PORT}`);