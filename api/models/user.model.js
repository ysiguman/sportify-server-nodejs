'use strict';
const db = require('./dbconnexion');
const sportService = require('./sport.model');
const table = 'user';

const User = {
    getAll: function () {
        const sql = "SELECT * FROM " + table;
        return db.run(sql);
    },
    get: function (id) {
        const sql = "SELECT * FROM " + table + " WHERE id = " + id;
        return db.run(sql);
    },
    getSportsByUser: (idUser) => {
        const sql =
            `SELECT s.id, s.name, link.level, link.comment, link.fk_sport_id FROM user u INNER JOIN sport_user link ON u.id = link.fk_user_id INNER JOIN sport s ON link.fk_sport_id = s.id WHERE u.id = ${idUser}`;
        return db.run(sql);
    },
    create: (data) => {
        const sql = `INSERT INTO ${table} (name, mail, password) VALUES ('${data.name}', '${data.mail}', '${data.password}')`;
        return db.runLastId(sql);
    },
    addSport: async (id, data) => {
        const sport = await sportService.getByName(data.name);
        const sql = `INSERT INTO sport_user(fk_user_id, fk_sport_id, level, comment) VALUES ('${id}', '${sport[0].id}', '${data.level}', '${data.comment}')`;
        console.log(sql);
        return db.run(sql);
    },
    connect: (mail, pass) => {
        const sql = `SELECT * FROM user WHERE mail = '${mail}' AND password = '${pass}'`;
        return db.run(sql);
    },
    getEvents: (id) => {
        const sql = 
        `SELECT e.id, e.name, e.level, e.members, e.date, sport.name as sport, e.principal, eu.state, SUM(eu.state) as registered, e.localisation
        FROM user u 
            INNER JOIN event_user eu ON eu.user_id = u.id 
            INNER JOIN event e ON e.id = eu.event_id 
            INNER JOIN sport ON e.sport = sport.id  
            WHERE u.id = ${id}`;
        return db.run(sql);
    },
    getEventsForUser: (id) => {
        const sql = `
        SELECT e.id, e.name, e.level, e.members, e.date, sport.name as sport, e.principal, eu.state, SUM(eu_count.state) as registered, e.localisation
        FROM event e
            LEFT JOIN event_user eu ON (eu.event_id = e.id AND eu.user_id = ${id})
            LEFT JOIN event_user eu_count ON eu_count.event_id = e.id
            LEFT JOIN user u ON eu.user_id = u.id 
            INNER JOIN sport ON e.sport = sport.id
            GROUP BY e.id`;
        return db.run(sql);
    }
};

module.exports=User;