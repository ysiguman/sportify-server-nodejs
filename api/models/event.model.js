'use strict';
const db = require('./dbconnexion');
const table = 'event';
const sportService = require('./sport.model');
const moment = require('moment');

const Event = {
    getAll: function () {
        const sql = "SELECT event.id, event.name, event.level, event.members, event.date, sport.name as sport, event.principal FROM event\n" +
            "\tINNER JOIN sport ON event.sport = sport.id";
        return db.run(sql);
    },
    get: function (id) {
        const sql = `SELECT event.id, event.name, event.level, event.members, event.date, sport.name as sport, event.principal, event.comment, SUM(eu.state) as registered, event.localisation 
            FROM event
            INNER JOIN sport ON event.sport = sport.id 
            INNER JOIN event_user eu ON eu.event_id = event.id 
            WHERE event.id = ${id}
            GROUP BY event.id`;
        return db.run(sql);
    },
    create: async (data, userId) => {
        const sport = await sportService.getByName(data.sport);
        let sql = `INSERT INTO ${table} (name, sport, level, members, comment, date, principal) VALUES ('${data.name}', '${sport[0].id}', '${data.level}', '${data.members}', '${data.comment}', '${moment(new Date(data.date)).format("YYYY-MM-DD HH:mm:ss")}', '${userId}')`;

        return db.runLastId(sql);
    },
    getUserOfEvent: (id) => {
        const sql = `SELECT u.id, u.name,   link.state FROM event INNER JOIN event_user link ON event.id = link.event_id INNER JOIN user u ON link.user_id = u.id WHERE event.id = ${id}`;
        return db.run(sql);
    },
    addUser: (id, data) => {
        const sql = `INSERT INTO event_user (user_id, state, event_id) VALUES (${data.user_id}, false, ${id})`;
        console.log(sql);
        return db.run(sql);
    },
    addPrincipal: (id, data) => {
        const sql = `INSERT INTO event_user (user_id, state, event_id) VALUES (${data.user_id}, true, ${id})`;
        console.log(sql);
        return db.run(sql);
    },
    removeUser: (id, data) => {
        const sql = `DELETE FROM event_user WHERE user_id = ${data.user_id} AND event_id = ${id}`;
        console.log(sql);
        return db.run(sql);
    },
    acceptUser: (id, data) => {
        const sql = `UPDATE event_user SET state = true WHERE event_user.user_id = ${data.user_id} AND event_user.event_id = ${id}`;
        console.log(sql);
        return db.run(sql);

    },
    revocUser: (id, data) => {
        const sql = `UPDATE event_user SET state = false WHERE event_user.user_id = ${data.user_id} AND event_user.event_id = ${id}`;
        return db.run(sql);

    },
    delete: (id) => {
        const sql = `DELETE FROM event WHERE id = ${id}`;
        return db.run(sql);
    }
};

module.exports=Event;