'use strict';
const db = require('./dbconnexion');
const table = 'sport';

const User = {
    getAll: function () {
        const sql = "SELECT * FROM " + table;
        return db.run(sql);
    },
    get: function (id) {
        const sql = "SELECT * FROM " + table + " WHERE id = " + id;
        return db.run(sql);
    },
    getByName: function (name) {
        const sql = "SELECT * FROM " + table + " WHERE name = '" + name + "'";
        return db.run(sql);
    }
};

module.exports=User;