const mysql = require('mysql');
let connection;
const config = require("../../config");

if (config) {
    connection = mysql.createPool({
        host: config.host,
        user: config.user,
        password: config.password,
        database: config.database
    });
} else {
    connection = mysql.createPool({
        host: "0.0.0.0",
        user: "root",
        password: "password",
        database: "sportify"
    });
}


function run (sql) {
  return new Promise((resolve, reject) => {
    connection.query(sql, (err, results) => {
      if (err) reject(err);
      resolve(results);

    })
  })
}

function runLastId (sql) {
  return new Promise((resolve, reject) => {
    connection.query(sql, (err, results) => {
      if (err) reject(err);
      resolve(results.insertId);

    })
  })
}
module.exports = {connection, run, runLastId};