# Docker file pour mettre en place le conteneur NodeJS
# Indique la source de l'image de base
FROM node:latest

# Mise en place du dossier de travail sur le conteneur
# (Par la suite le pwd correspond à /usr/src/app)
WORKDIR /usr/src/app

# Importe les fichiers nodeJS dans le container
COPY package*.json ./

# Mets à jour les dépendances node
RUN npm install

# Copie les dossier du projet sur le conteneur
COPY . .

# Expose le port 8080 du conteur sur local
EXPOSE 8080

CMD ["npm", "start"]